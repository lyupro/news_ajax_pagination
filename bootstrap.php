<?php

require_once $_SERVER['DOCUMENT_ROOT']."/app/config/MainConfig.php";

require_once __ROOT__."/vendor/autoload.php";

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

/**
 *
 */
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('Logger');
$log->pushHandler(new StreamHandler('logs/logs.log', Logger::WARNING));

require_once __PLUGINS__."rb-mysql.php";
require_once __CONFIG__."DBConfig.php";

require_once __PLUGINS__."twigExtends.php";

require_once __CONFIG__."RoutesConfig.php";

require_once __APP__."router.php";

