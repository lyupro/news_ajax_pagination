
$('body').on('click','.load-news', function(e) {
    e.preventDefault();

    var pageID = $(this).data('id');

    showNewsByPageID(pageID);
});

function showNewsByPageID(pageID) {

    var newsID;
    $('.btn-delete').each(function(key, val){
        newsID = $(`.btn-delete`).data('id');
        $(`tr[data-id=${newsID}]`).detach();
    });

    var oldPage;
    $('.load-news').each(function(key, val){
        oldPage = $(`.load-news`).data('id');
        $(`.page-item`).detach();
    });

    ajaxShowNewsByPageID(pageID);
}

function ajaxShowNewsByPageID(pageID) {

    $.ajax({
        'url':'/app/ajax/ajax.php',
        'type':'POST',
        'data':{'ajaxAction':1, 'pageID':pageID},
        'success':function (res) {
            console.log(res);
            let result = JSON.parse(res);
            console.log(result.table);
            $('.ajaxTable').append(result.table).hide().fadeIn(500);;

            $('.ajaxPagination').append(result.pagination);
        }
    })
}