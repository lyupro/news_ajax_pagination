

$('body').on('click','.btn-delete', function(e) {
    e.preventDefault();

    var newsID = $(this).data('id');

    deleteNewsByID(newsID);
});

function deleteNewsByID(newsID){

    $(`tr[data-id=${newsID}]`).detach();

    ajaxDeleteNewsByID(newsID);
}

function ajaxDeleteNewsByID(newsID) {

    $.ajax({
        'url':'/app/ajax/ajax.php',
        'type':'POST',
        'data':{'ajaxAction':2, 'newsID':newsID},
        'success':function (res) {
        }
    })
}