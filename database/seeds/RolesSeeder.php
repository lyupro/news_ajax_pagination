<?php


use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class RolesSeeder extends AbstractSeed
{
    private $roles = [
        "User",
        "Moderator",
        "Administrator"
    ];

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {

        $faker = Factory::create();
        $data = [];

        for( $i = 0; $i < 3; $i++){
            $data[] = [
                'role'              => $this->roles[$i],
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ];
        }

        $this->table('roles')->insert($data)->save();
    }
}
