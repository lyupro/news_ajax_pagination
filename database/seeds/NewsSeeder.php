<?php


use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class NewsSeeder extends AbstractSeed
{

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Factory::create();
        $data = [];
        for( $i = 0; $i < 30; $i++){
            $random = $faker->numberBetween(200, 500);
            $data[] = [
                'user_id'           => $faker->numberBetween(1, 3),
                'category_id'       => $faker->numberBetween(1, 3),
                'title'             => $faker->sentence,
                'text'              => $faker->text($random),
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ];
        }

        $this->table('news')->insert($data)->save();
    }
}
