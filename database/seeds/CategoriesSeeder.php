<?php


use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class CategoriesSeeder extends AbstractSeed
{
    private $roles = [
        "News",
        "Events",
        "Parties"
    ];

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {

        $faker = Factory::create();
        $data = [];

        for( $i = 0; $i < 3; $i++){
            $data[] = [
                'name'              => $this->roles[$i],
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ];
        }

        $this->table('categories')->insert($data)->save();
    }
}
