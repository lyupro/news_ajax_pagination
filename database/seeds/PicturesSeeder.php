<?php


use Faker\Factory;
use Phinx\Seed\AbstractSeed;

class PicturesSeeder extends AbstractSeed
{

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = Factory::create();
        $data = [];
        for( $i = 0; $i < 30; $i++){
            $data[] = [
                'news_id'           => $faker->unique()->numberBetween(157, 247),
                'picture'           => $faker->image('tmp', 1280, 720),
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
            ];
        }

        $this->table('pictures')->insert($data)->save();
    }
}
