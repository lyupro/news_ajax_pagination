<?php

use Phinx\Migration\AbstractMigration;

class CreateTableUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    /**
     * Migrate Up
     */
    public function up()
    {
        $table = $this->table('users');
        $table->addColumn('name', 'string', ['limit' => 100])
            ->addColumn('email', 'string', ['limit' => 100])
            ->addColumn('role_id', 'integer', ['limit' => 11])
            ->addForeignKey('role_id',
                'roles',
                'id',
                ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addTimestamps()
            ->create();
    }

    /**
     * Migrate Down
     */
    public function down()
    {
        $this->table('users')->drop()->save();
    }
}
