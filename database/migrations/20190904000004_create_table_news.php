<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class CreateTableNews extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */

    /**
     * Migrate Up
     */
    public function up()
    {
        $table = $this->table('news');
        $table->addColumn('user_id', 'integer', ['limit' => 11])
            ->addForeignKey('user_id',
                'users',
                'id',
                ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])

            ->addColumn('category_id', 'integer', ['limit' => 11])
            ->addForeignKey('category_id',
                'categories',
                'id',
                ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])

            ->addColumn('title', 'string', ['limit' => 200])
            ->addColumn('text', 'text', ['limit' => MysqlAdapter::TEXT_LONG])

            ->addTimestamps()
            ->create();
    }

    /**
     * Migrate Down
     */
    public function down()
    {
        $this->table('news')->drop()->save();
    }
}
