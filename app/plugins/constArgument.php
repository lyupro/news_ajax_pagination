<?php

namespace App\Plugins;

use App\Controllers\ErrorController;

class constArgument extends ErrorController
{
    /**
     * @return bool|int
     */
    public static function checkConst()
    {
        if( isset($_SESSION['ARGUMENT']) && !empty($_SESSION['ARGUMENT']) && is_numeric($_SESSION['ARGUMENT'])){
            return intval($_SESSION['ARGUMENT']);
        }else{
            return false;
        }
    }

}