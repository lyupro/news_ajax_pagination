<?php

use App\Models\News;
use App\Models\Picture;
use App\Controllers\NewsController;

require_once $_SERVER['DOCUMENT_ROOT']."/bootstrap.php";

require_once __MODELS__."News.php";
require_once __MODELS__."Picture.php";
require_once __CONTROLLERS__."NewsController.php";

if( isset($_POST["ajaxAction"]) && $_POST["ajaxAction"] == 1){

    $_SESSION['ARGUMENT'] = $_POST["pageID"];

    $news = new NewsController();
    echo $news->indexJSON();

}elseif( isset($_POST["ajaxAction"]) && $_POST["ajaxAction"] == 2){
    News::deleteNewsByID($_POST["newsID"]);
    Picture::deletePictureByID($_POST["newsID"]);
}