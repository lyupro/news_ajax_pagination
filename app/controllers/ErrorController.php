<?php

namespace App\Controllers;

use App\Plugins\twigExtends;

class ErrorController extends twigExtends
{
    /**
     * @return string
     */
    public function error404()
    {
        header("HTTP/1.0 404 Not Found");

        $getTwig = self::twig();
        return $getTwig->render("error404.html");
    }
}