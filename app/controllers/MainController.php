<?php

namespace App\Controllers;

use App\Plugins\twigExtends;

class MainController extends twigExtends
{
    /**
     * @return string
     */
    public function index()
    {
        $getTwig = self::twig();
        return $getTwig->render("index.html", ["content" => "Hello World!"]);
    }
}