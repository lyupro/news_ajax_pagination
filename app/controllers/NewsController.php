<?php

namespace App\Controllers;

use App\Models\News;
use App\Models\Page;
use App\Models\Picture;
use App\Models\Category;
use App\Models\User;
use App\Models\Role;
use App\Plugins\constArgument;
use App\Plugins\twigExtends;

use App\Controllers\ErrorController;

require_once __MODELS__."News.php";
require_once __MODELS__."Page.php";
require_once __MODELS__."Picture.php";
require_once __MODELS__."Category.php";
require_once __MODELS__."User.php";
require_once __MODELS__."Role.php";

require_once __CONTROLLERS__."ErrorController.php";
require_once __PLUGINS__."constArgument.php";

class NewsController extends twigExtends
{
    /**
     * @return string
     */
    public function index()
    {
        if( isset($_SESSION['ARGUMENT']) && !empty($_SESSION['ARGUMENT']) && is_numeric($_SESSION['ARGUMENT'])){
            $counter = intval($_SESSION['ARGUMENT']);
            $counter = ($counter * 10) -10;
        }else{
            $counter = 0;
        }

        $newsArray = News::getNews($counter);

        if(!empty($newsArray)){
            foreach ($newsArray as $index) {
                $news .= "<tr data-id=".$index['id']."> 
                            <th scope='row'>".$index['id']."</th>
                            <td>".$index['title']."</td>
                            <td>".$index['created_at']."</td>
                            <td>
                                <a href='/news/show/".$index['id']."' class='btn btn-outline-success'>Show</a>
                                <a href='/news/edit/".$index['id']."' class='btn btn-outline-warning'>Edit</a>
                                <a href='' data-id='".$index['id']."' class='btn btn-outline-danger btn-delete'>Delete</a>
                            </td>
                        </tr>";
            }

            $pages = Page::getPagesNews();
            $p = ceil($pages[0]['pages'] / 10);
            for($i = 1; $i <= $p; $i++){
                $pagination .= "
                <li class='page-item'>
                    <a data-id='".$i."' class='page-link load-news' href='/news/page/{$i}'>{$i}</a>
                </li>";
            }

            $getTwig = self::twig();
            return $getTwig->render("news.html", ['news' => $news, 'pagination' => $pagination]);
        }else{
            $error404 = new ErrorController();
            return $error404->error404();
        }
    }

    /**
     * @return false|string
     */
    public function indexJSON()
    {
        if( isset($_SESSION['ARGUMENT']) && !empty($_SESSION['ARGUMENT']) && is_numeric($_SESSION['ARGUMENT'])){
            $counter = intval($_SESSION['ARGUMENT']);
            $counter = ($counter * 10) -10;
        }else{
            $counter = 0;
        }

        $newsArray = News::getNews($counter);

        if(!empty($newsArray)){
            $newsArrayJSON = [];
            foreach ($newsArray as $index) {
                $newsArrayJSON['table'] .= "<tr data-id=".$index['id']."> 
                            <th scope='row'>".$index['id']."</th>
                            <td>".$index['title']."</td>
                            <td>".$index['created_at']."</td>
                            <td>
                                <a href='/news/show/".$index['id']."' class='btn btn-outline-success'>Show</a>
                                <a href='/news/edit/".$index['id']."' class='btn btn-outline-warning'>Edit</a>
                                <a href='' data-id='".$index['id']."' class='btn btn-outline-danger btn-delete'>Delete</a>
                            </td>
                        </tr>";
            }

            $pages = Page::getPagesNews();
            $p = ceil($pages[0]['pages'] / 10);
            for($i = 1; $i <= $p; $i++){
                $newsArrayJSON['pagination'] .= "
                <li class='page-item'>
                    <a data-id='".$i."' class='page-link load-news' href='/news/page/{$i}'>{$i}</a>
                </li>";
            }

            return json_encode($newsArrayJSON);
        }
    }

    /**
     * @return string
     */
    public function create()
    {
        $categories = Category::getCategories();

        foreach ($categories as $category) {
            $options .= "<option value='".$category->id."'>".$category->name."</option>";
        }
        
        $getTwig = self::twig();
        return $getTwig->render("create.html", ['categories' => $options]);
    }

    public function store()
    {
        $news_id = News::saveNews();
        Picture::savePicture($news_id);

        return $this->index();
    }

    public function show()
    {
        $news_id = constArgument::checkConst();

        // News Section
        $news = News::getNewsByID($news_id);
        if( $news->id != $news_id ){
            $error404 = new ErrorController();
            return $error404->error404();
        }

        // Categories Section
        $category = Category::getCategoryByID($news->category_id);
        $optCategory .= "Category: <b value='".$category->id."'>".$category->name."</b>";

        // Pictures Section
        $picture = Picture::getPictureByID($news->id);
        if($picture === null){
            $optPicture .= "<b>None</b>";
        }else{
            $optPicture .= "\\".$picture->picture;
        }

        $getTwig = self::twig();
        return $getTwig->render("show.html", [
            'news_id' => $news->id,
            'title' => $news->title,
            'text' => $news->text,
            'category' => $optCategory,
            'picture' => $optPicture,
            'created_at' => $news->created_at,
        ]);
    }

    public function edit()
    {
        $news_id = constArgument::checkConst();

        // News Section
        $news = News::getNewsByID($news_id);
        if( $news->id != $news_id ){
            $error404 = new ErrorController();
            return $error404->error404();
        }

        // Categories Section
        $category = Category::getCategoryByID($news->category_id);
        $optCategory .= "<b value='".$category->id."'>".$category->name."</b>";

        $categories = Category::getCategories();
        foreach ($categories as $cat) {
            $optCategories .= "<option value='".$cat->id."'>".$cat->name."</option>";
        }

        // Pictures Section
        $picture = Picture::getPictureByID($news->id);
        if($picture === null){
            $optPicture .= "<b>None</b>";
        }else{
            $optPicture .= "\\".$picture->picture;
        }

        $pictures = Picture::getPictures();
        foreach ($pictures as $pict) {
            $optPictures .= "<option value='".$pict->id."'>".$pict->picture."</option>";
        }

        $getTwig = self::twig();
        return $getTwig->render("edit.html", [
            'news_id' => $news->id,
            'title' => $news->title,
            'text' => $news->text,
            'category' => $optCategory,
            'categories' => $optCategories,
            'picture' => $optPicture,
            'pictures' => $optPictures
        ]);
    }

    /**
     * @return string
     */
    public function update()
    {
        $news_id = constArgument::checkConst();

        $news = News::updateNews($news_id);

        if( $news == false ){
            $error404 = new ErrorController();
            return $error404->error404();
        }

        Picture:: updatePicture($news_id);

        return $this->edit();
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        News::deleteNewsByID($id);
    }

    /**
     * @return string
     */
    public function deleteAll()
    {
        Picture::deletePictures();
        News::deleteNews();
        Category::deleteCategories();
        User::deleteUsers();
        Role::deleteRoles();
        return $this->index();
    }
}