<?php

namespace App;

use App\Plugins\twigExtends;

$getUri = parse_url(__URI__, PHP_URL_PATH);
$getUri = trim($getUri,"/");

$sections = explode("/", $getUri);

if(count($sections) < 3){
    if( !empty($getUri)){
        if(array_key_exists($getUri, $routes)){
            $getController = $routes[$getUri];
        }
    }else{
        $getController = $routes['main'];
    }

    unset($_SESSION['ARGUMENT']);

}elseif(count($sections) == 3){
    $key = $sections[0]."/".$sections[1]."/{id}";

    $_SESSION['ARGUMENT'] = $sections[2];

    if(array_key_exists($key , $routes)){
        $getController = $routes[$key];
    }
}else{
    if(array_key_exists("error404", $routes)){
        $getController = $routes['error404'];
    }

    unset($_SESSION['ARGUMENT']);
}



$array = explode("|", $getController);
$filePath = str_replace("\\","/",$array[0]);
$class = $array[0];
$method = $array[1];

$getController = $filePath.".php";

try{
    if( !file_exists($getController)){
        throw new \Exception('Controller is not exists!');
    }else{
        require_once $filePath.".php";
    }
}catch (\Exception $exception){
    $except = $exception->getMessage()."<br>";
}

try{
    if( !class_exists($class)){
        throw new \Exception('Class is not exists!');
    }
}catch (\Exception $exception){
    $except .= $exception->getMessage();
}

if(isset($except)){
    $getTwig = twigExtends::twig();
    $getMenu = $getTwig->render("menu.html");
    $html = $getTwig->render("header.html", ["menu"=>$getMenu]);
    $html .= $getTwig->render("index.html");
    $html .= $getTwig->render("footer.html", ["exception"=>$except]);
    return;
}


if( class_exists($class) ){
    $getTwig = twigExtends::twig();
    $getMenu = $getTwig->render("menu.html");
    $html = $getTwig->render("header.html", ["menu"=>$getMenu]);

    $newObj = new $class;
    $html .= $newObj->$method();
    $html .= $getTwig->render("footer.html");
}