<?php

use App\Plugins\twigExtends;

try{
    R::setup("mysql:host={$_ENV['dbhost']};dbname={$_ENV['dbname']}", $_ENV['dblogin'], $_ENV['dbpass']);

    if(!R::testConnection()){
        $log->error("Database connection is lost!");
        throw new Exception('Database connection is lost!');
    }
}catch (Exception $exception){
    $except = $exception->getMessage();

    $getTwig = twigExtends::twig();
    $getMenu = $getTwig->render("menu.html");
    $html .= $getTwig->render("index.html", ["exception"=>$except]);
    exit();
}