<?php

namespace App\Config;

$routes= [];
$routes['main'] = "App\\Controllers\\MainController|index";
$routes['news'] = "App\\Controllers\\NewsController|index";
$routes['news/add'] = "App\\Controllers\\NewsController|create";
$routes['news/save'] = "App\\Controllers\\NewsController|store";
$routes['news/page/{id}'] = "App\\Controllers\\NewsController|index";
$routes['news/edit/{id}'] = "App\\Controllers\\NewsController|edit";
$routes['news/update/{id}'] = "App\\Controllers\\NewsController|update";
$routes['news/show/{id}'] = "App\\Controllers\\NewsController|show";
$routes['news/delete/{id}'] = "App\\Controllers\\NewsController|delete";
$routes['news/deleteAll'] = "App\\Controllers\\NewsController|deleteAll";

$routes['error404'] = "App\\Controllers\\ErrorController|error404";