<?php

namespace App\Models;

class News
{
    /**
     * @param int $counter
     * @return array
     */
    public static function getNews(int $counter)
    {
        return \R::getAll("SELECT * FROM news LIMIT $counter, 10");
    }

    /**
     * @param int $id
     * @return \RedBeanPHP\OODBBean
     */
    public static function getNewsByID(int $id)
    {
        return \R::load("news", $id);
    }

    /**
     * @return array|\RedBeanPHP\OODBBean
     */
    public static function addNews()
    {
        return \R::dispense("news");
    }

    /**
     * @return int|string
     */
    public static function saveNews()
    {
        $news = \R::dispense( "news" );
        $news->user_id = 1;
        $news->category_id = $_POST['category_id'];
        $news->title = $_POST['title'];
        $news->text = $_POST['text'];

        return \R::store( $news );
    }

    /**
     * @param $id
     * @return bool|int|string
     */
    public static function updateNews($id)
    {
        $news = \R::load( 'news', $id ); //reloads our news

        if( $news->id != $id ){
            return false;
        }

        $news->user_id = 1;

        if($news->category_id != $_POST['category_id']){
            $news->category_id = $_POST['category_id'];
        }

        $news->title = $_POST['title'];
        $news->text = $_POST['text'];

        return \R::store( $news );
    }

    /**
     * @param int $id
     */
    public static function deleteNewsByID(int $id)
    {
        $news = \R::findOne('news', 'id=?', [$id]);
        return \R::trash( $news );
    }

    /**
     * @return bool
     */
    public static function deleteNews()
    {
        return \R::wipe( 'news' );
    }
}