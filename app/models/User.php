<?php

namespace App\Models;

class User
{
    /**
     * @return bool
     */
    public static function deleteUsers()
    {
        return \R::wipe( 'users' );
    }
}