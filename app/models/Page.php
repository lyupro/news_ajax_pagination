<?php

namespace App\Models;

class Page
{
    /**
     * @return array
     */
    public static function getPagesNews()
    {
        return \R::getAll("SELECT COUNT(*) AS pages FROM news");
    }
}