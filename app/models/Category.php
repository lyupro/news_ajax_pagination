<?php

namespace App\Models;

class Category
{
    /**
     * @return array
     */
    public static function getCategories()
    {
        return \R::findAll("categories");
    }

    /**
     * @param int $id
     * @return \RedBeanPHP\OODBBean
     */
    public static function getCategoryByID(int $id)
    {
        return \R::load("categories", $id);
    }

    /**
     * @return bool
     */
    public static function deleteCategories()
    {
        return \R::wipe( "categories" );
    }
}