<?php

namespace App\Models;

class Role
{
    /**
     * @return bool
     */
    public static function deleteRoles()
    {
        return \R::wipe( 'roles' );
    }
}