<?php

namespace App\Models;

class Picture
{
    /**
     * @return array
     */
    public static function getPictures()
    {
        return \R::findAll("pictures");
    }

    /**
     * @param int $news_id
     * @return NULL|\RedBeanPHP\OODBBean
     */
    public static function getPictureByID(int $news_id)
    {
        return \R::findOne('pictures', 'news_id=?', [$news_id]);
    }

    /**
     * @param $news_id
     * @param bool $move
     * @param bool $fromFile
     * @return int|string
     */
    public static function savePicture($news_id, $move = true, $fromFile = true)
    {
        if($move == true){
            move_uploaded_file($_FILES['picture']['tmp_name'], "tmp/".$_FILES['picture']['name']);
        }

        $pictures = \R::dispense("pictures");

        $pictures->news_id = $news_id;

        if($fromFile == true){
            $pictures->picture = "tmp\\".$_FILES['picture']['name'];
        }else{
            $pictureID = $_POST['picture'];
            $getPicture = \R::load( 'pictures', $pictureID ); //reloads our book
            $pictures->picture = $getPicture->picture;
        }

        return \R::store( $pictures );
    }

    /**
     * @param $news_id
     * @return int|string
     */
    public static function updatePicture($news_id)
    {
        $pictureID = $_POST['picture'];

        $pictures = \R::findOne('pictures', 'news_id=?', [$news_id]);
        if($pictures === null){
            return self::savePicture($news_id, false, false);
        }

        $getPicture = \R::load( 'pictures', $pictureID ); //reloads our pictures

        if( $pictures->picture != $getPicture->picture){
            $pictures->picture = $getPicture->picture;
            return \R::store( $pictures );
        }
    }

    /**
     * @param int $news_id
     */
    public static function deletePictureByID(int $news_id)
    {
        $picture = \R::findOne('pictures', 'news_id=?', $news_id);
        return \R::trash( $picture );
    }

    /**
     * @return bool
     */
    public static function deletePictures()
    {
        return \R::wipe( 'pictures' );
    }
}